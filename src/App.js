import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Routes
import ScrollToTop from './components/ScrollToTop';
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home';
import Chatbot from './pages/Chatbot';
import Individual from './pages/Individual';
import Professional from './pages/Professional';
import Recruitment from './pages/Recruitment';
import Appointment from './pages/Appointment';
import Partner from './pages/Partner';
import Resources from './pages/Resources';
import Contact from './pages/Contact';

// GA
import ReactGA from 'react-ga';
ReactGA.initialize('UA-92200221-1');
ReactGA.pageview(window.location.pathname + window.location.search);

class App extends React.Component {
	render() {
		return (
			<Router>
				<Header />
				<ScrollToTop>
					<Route path="/" exact component={Home} />
					<Route path="/chatbot" component={Chatbot} />
					<Route path="/individual" component={Individual} />
					<Route path="/professional" component={Professional} />
					<Route path="/recruitment" component={Recruitment} />
					<Route path="/appointment" component={Appointment} />
					<Route path="/partner" component={Partner} />
					<Route path="/resources" component={Resources} />
					<Route path="/contact" component={Contact} />
				</ScrollToTop>
				<Footer />
			</Router>
		);
	}
}

export default App;
