export const individualOpinions = [
	{
		imgName: 'marieandguillaume',
		name: 'Marie et Guillaume L.',
		place: 'Région de Paris',
		comment:
			'Un accompagnement parfait ! Merci de nous avoir aussi bien accompagné dans la concrétisation de nos projets.'
	},
	{
		imgName: 'julie',
		name: 'Julie G.',
		place: 'Région de Montpellier',
		comment:
			"Equipe dynamique et très professionnelle qui a su être à l'écoute de notre projet et y apporter une réponse rapide et pertinente."
	},
	{
		imgName: 'charlotteandcyrille',
		name: 'Charlotte et Cyrille L.',
		place: 'Région de Montpellier',
		comment:
			'Un service exceptionnel, des interlocuteurs très professionnels, une gestion parfaite, on ne peut rien dire de moins !'
	}
];

export const professionalOpinions = [
	{
		imgName: 'ludovic',
		name: 'Ludovic L.',
		position: 'Fondateur site immobilier neuf',
		place: 'Région Montpellier',
		comment:
			"L'application est simple d'utilisation mais surtout elle est très intuitive, de plus l'équipe est compétente et réactive c'est top :)"
	},
	{
		imgName: 'jessica',
		name: 'Jessica K.',
		position: 'Responsable développement promotion immobilière',
		place: 'Région Paris',
		comment:
			"Une équipe sérieuse qui vise le meilleur résultat pour ses clients avec la mise à disposition d'une application que l'on trouve nulle part ailleurs. Un grand merci pour votre réelle écoute et disponibilité !"
	},
	{
		imgName: 'emilie',
		name: 'Emilie C.',
		position: 'Responsable projets CCMI',
		place: 'Région Montpellier',
		comment:
			"L'application est une véritable alliée pour mes ventes. Facile d'utilisation, elle permet des simulations rapides et fiables. La prise de contact est réactive, sérieuse, et mes clients se sentent tout de suite en confiance. Je peux ainsi les diriger vers un projet complètement adapté. SWITFI est pour moi un vrai partenaire de qualité, sur lequel je peux m'appuyer pour une satisfaction client optimale."
	}
];
