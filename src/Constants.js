// Opinion System
export const opinionUrl = 'https://new.api.opinionsystem.fr/v2/company/rating?api_key=40a6a649cb9d8e29e2acecd79893baad';

// Switfi
export const switfiTokenUrl = 'https://api.dev.switfi.fr/oauth/v2/token';
export const switfiAuthorization =
	'Basic MV8xcGUyNWo1Mm81NDBrY3N3b2t3c284NGtnZ2N3ZzRrczg0Y3drczh3Z3M4c3dndzh3Yzo1MG13NnM2cmZ6YzQ4Z293NGdvdzRjZ293a3M4d2c4a2M4OHdnNGtvazBzb2c0MHdjYw==';
export const switfiContactsUrl = 'https://api.dev.switfi.fr/app_dev.php/api/incoming_contacts';
export const switfiAppointmentUrl = 'https://api.dev.switfi.fr/app_dev.php/api/appointment/incoming_contact/';

// Mail Chimp
export const mailChimpNewsletterUrl = 'https://us15.api.mailchimp.com/3.0/lists/';
export const mailChimpAuthorization = 'c3dpdGZpOmRmNzYxMmNmYmI4MzhiMmEzMTQ0YmRkODU4YzM2Mzc3LXVzMTU=';

// SMS Partner
export const smsPartnerUrl = 'https://api.smspartner.fr/v1/send';
