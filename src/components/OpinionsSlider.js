import React, { Component } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { withRouter } from 'react-router';

import * as Opinions from '../Opinions';
import Quote from '../assets/icons/quote.svg';

class OpinionsSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			opinions: []
		};
	}
	componentDidMount() {
		switch (this.props.location.pathname) {
			case '/professional':
				this.setState({ opinions: Opinions.professionalOpinions });
				break;
			default:
				this.setState({ opinions: Opinions.individualOpinions });
		}
	}

	render() {
		const images = require.context('../assets/imgs/opinions', true);

		const cardsList = this.state.opinions.map((opinion, index) => {
			let imgsrc = images(`./${opinion.imgName}.jpg`);
			return (
				<div className="block opinion-slider__item" key={index}>
					<div className="d-flex align-items-start">
						<div className="opinion-slider__img" style={{ backgroundImage: `url(${imgsrc})` }} />
						<img src={Quote} alt="Avis" className="opinion-slider__icon" />
						<div className="d-flex flex-column mb-4">
							<p className="opinion-slider__name">{opinion.name}</p>
							<div className="d-flex flex-column">
								<p className="opinion-slider__place">{opinion.position}</p>
								<p className="opinion-slider__place">{opinion.place}</p>
							</div>
						</div>
					</div>
					<p className="opinion-slider__comment">{opinion.comment}</p>
				</div>
			);
		});

		const responsive = {
			desktop: {
				breakpoint: { max: 100000, min: 1200 },
				items: 1
			},
			tablet: {
				breakpoint: { max: 1200, min: 768 },
				items: 1
			},
			mobile: {
				breakpoint: { max: 768, min: 0 },
				items: 1
			}
		};

		return (
			<div>
				<div className="d-none d-lg-block">
					<Carousel
						centerMode
						responsive={responsive}
						additionalTransfrom={0}
						arrows={false}
						autoPlaySpeed={3000}
						containerClass="opinion-slider"
						itemClass=""
						dotListClass=""
						draggable
						focusOnSelect
						infinite
						keyBoardControl
						minimumTouchDrag={80}
						renderDotsOutside
						showDots={false}
						sliderClass=""
						slidesToSlide={1}
						swipeable
					>
						{cardsList}
					</Carousel>
				</div>

				{/* MOBILE CAROUSEL */}
				<div className="d-lg-none">
					<Carousel
						responsive={responsive}
						additionalTransfrom={0}
						arrows={false}
						autoPlaySpeed={3000}
						containerClass="opinion-slider"
						itemClass=""
						dotListClass="opinion-slider__dots"
						draggable
						focusOnSelect
						infinite
						keyBoardControl
						minimumTouchDrag={80}
						sliderClass=""
						slidesToSlide={1}
						swipeable
						showDots
						renderDotsOutside
					>
						{cardsList}
					</Carousel>
				</div>
			</div>
		);
	}
}
export default withRouter(OpinionsSlider);
