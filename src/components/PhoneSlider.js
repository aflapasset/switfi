import React, { Component } from 'react';

import phone from '../assets/imgs/phoneSlider/phone.png';
import parisGood from '../assets/imgs/phoneSlider/paris-good.jpg';
import montpellierGood from '../assets/imgs/phoneSlider/montpellier-good.jpg';
import checked from '../assets/icons/checked-green.svg';
import marker from '../assets/icons/marker.svg';

import appStore from '../assets/icons/app-store.svg';
import googlePlay from '../assets/icons/google-play.png';
import switfiLogo from '../assets/logos/switfi-black.svg';

class PhoneSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			sliderPos: -178,
			goods: [
				{
					img: parisGood,
					city: 'Paris'
				},
				{
					img: montpellierGood,
					city: 'Montpellier'
				},
				{
					img: parisGood,
					city: 'Paris'
				},
				{
					img: montpellierGood,
					city: 'Montpellier'
				}
			],
			currentGoods: [],
			isParisBg: false
		};
	}

	componentDidMount() {
		this.setState({ currentGoods: this.state.goods });
		this.addGoods();
		this.slide();
	}

	slide() {
		const sliderGap = 200;
		let counter = 0;
		window.setInterval(() => {
			if (this.isEven(counter)) {
				this.addGoods();
			}
			this.setState((prevState) => {
				return {
					sliderPos: (prevState.sliderPos -= sliderGap),
					isParisBg: !prevState.isParisBg
				};
			});
			counter += 1;
		}, 3000);
	}

	isEven(n) {
		return n % 2 === 0;
	}

	addGoods() {
		let currentGoods = this.state.currentGoods.slice();
		let newCurrentGoods = currentGoods.concat(this.state.goods);
		this.setState({
			currentGoods: newCurrentGoods
		});
	}

	render() {
		const goodsList = this.state.currentGoods.map((good, index) => {
			return (
				<div className="phoneSlider__slider__item-container" key={index}>
					<div className="d-flex ml-4 mb-2 align-items-center">
						<img src={marker} alt="Position" />
						<p className="phoneSlider__city">{good.city}</p>
					</div>
					<div className="phoneSlider__slider__item">
						<div className="phoneSlider__goods" style={{ backgroundImage: `url(${good.img})` }} />
						<div className="phoneSlider__footer">
							<img src={checked} alt="" />
							<p className="phoneSlider__text">financé grâce à Switfi</p>
						</div>
					</div>
				</div>
			);
		});

		return (
			<section className="phoneSlider">
				<div className="phoneSlider__phone-container">
					{this.state.isParisBg ? <div className="phoneSlider__bg phoneSlider__bg--paris" /> : null}
					{!this.state.isParisBg ? <div className="phoneSlider__bg phoneSlider__bg--montpellier" /> : null}
					<div className="phoneSlider__slider__container">
						<div
							className="phoneSlider__slider"
							style={{ transform: `translateX(${this.state.sliderPos}px)` }}
						>
							{goodsList}
						</div>
					</div>
					<img src={phone} alt="iphone" className="phoneSlider__phone" />
				</div>
				<div className="phoneSlider__cta">
					<p className="phoneSlider__hint">Téléchargez l’application gratuite</p>
					<img src={switfiLogo} alt="Logo Switfi" className="phoneSlider__logo" />
					<div className="d-flex mt-4 mt-lg-5 justify-content-center justify-content-lg-start">
						<a href="https://apps.apple.com/us/app/cr%C3%A9dit-immobilier-switfi/id1330597688">
							<img
								className="imageHeader__appLink__logo mr-1"
								alt="Télécharger dans l'App Store"
								src={appStore}
							/>
						</a>
						<a href="https://play.google.com/store/apps/details?id=fr.switfi.android.companion&hl=en_US&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1">
							<img
								className="imageHeader__appLink__logo ml-1"
								alt="Disponible sur Google Play"
								src={googlePlay}
							/>
						</a>
					</div>
				</div>
			</section>
		);
	}
}

export default PhoneSlider;
