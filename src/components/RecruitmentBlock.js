import React, { Component } from 'react';
import Quote from '../assets/icons/quote.svg';

class RecruitmentBlock extends Component {
	render() {
		return (
			<div className="block recruitmentBlock">
				<div className="recruitmentBlock__img">
					<img
						src={require('../assets/imgs/recruitment/blocks/' + this.props.name + '.png')}
						alt={this.props.name}
					/>
				</div>
				<div className="recruitmentBlock__data">
					<img src={Quote} alt="Avis" className="recruitmentBlock__icon" />
					<h4 className="recruitmentBlock__name">{this.props.name}</h4>
					<span className="recruitmentBlock__position">{this.props.position}</span>
					<p className="recruitmentBlock__text">{this.props.text}</p>
				</div>
			</div>
		);
	}
}

export default RecruitmentBlock;
