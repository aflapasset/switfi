import React, { Component } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';

import creditAgricole from '../assets/logos/credit-agricole.svg';
import axa from '../assets/logos/axa.svg';
import banquePalatine from '../assets/logos/banque-palatine.png';
import banquePopulaire from '../assets/logos/banque-populaire.svg';
import cic from '../assets/logos/cic.png';
import bred from '../assets/logos/bred.png';
import creditDuNord from '../assets/logos/credit-du-nord.svg';
import lcl from '../assets/logos/lcl.png';
import societeGenerale from '../assets/logos/societe-generale.svg';
import smc from '../assets/logos/societe-marseillaise-de-credit.png';
import caisseEpargne from '../assets/logos/caisse-epargne.png';

class LogoSlider extends Component {
	render() {
		const responsive = {
			desktop: {
				breakpoint: { max: 100000, min: 1200 },
				items: this.props.logosNb
			},
			tablet: {
				breakpoint: { max: 1200, min: 768 },
				items: this.props.logosNb - 2
			},
			mobile: {
				breakpoint: { max: 768, min: 0 },
				items: 3
			}
		};

		return (
			<div>
				<div className="d-none d-lg-block">
					<Carousel
						responsive={responsive}
						additionalTransfrom={0}
						arrows={false}
						autoPlay={false}
						autoPlaySpeed={1}
						centerMode={false}
						customTransition="all 6s linear"
						draggable
						focusOnSelect={false}
						infinite
						containerClass="logos-slider"
						itemClass="logos-slider__item"
						showDots={false}
						sliderClass=""
						slidesToSlide={1}
						swipeable
						transitionDuration={6000}
					>
						<div className="logos-slider__img" style={{ backgroundImage: `url(${creditAgricole})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${axa})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${banquePalatine})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${banquePopulaire})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${cic})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${bred})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${creditDuNord})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${lcl})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${societeGenerale})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${smc})` }} />
						<div className="logos-slider__img" style={{ backgroundImage: `url(${caisseEpargne})` }} />
					</Carousel>
				</div>
				<div className="d-lg-none">
					<Carousel
						responsive={responsive}
						additionalTransfrom={0}
						arrows={false}
						autoPlay
						autoPlaySpeed={1}
						centerMode={false}
						customTransition="all 2s linear"
						draggable
						focusOnSelect={false}
						infinite
						containerClass="logos-slider"
						itemClass="logos-slider__item"
						showDots={false}
						sliderClass=""
						slidesToSlide={1}
						swipeable
						transitionDuration={2000}
					>
						<img src={creditAgricole} alt="Credit Agricole" />
						<img src={axa} alt="AXA" />
						<img src={banquePalatine} alt="Banque Palatine" />
						<img src={banquePopulaire} alt="Banque Populaire" />
						<img src={cic} alt="CIC" />
						<img src={bred} alt="BRED" />
						<img src={creditDuNord} alt="Credit Du Nord" />
						<img src={lcl} alt="LCL" />
						<img src={societeGenerale} alt="Societe Generale" />
						<img src={smc} alt="SMC" />
						<img src={caisseEpargne} alt="Caisse d'Epargne" />
					</Carousel>
				</div>
			</div>
		);
	}
}

export default LogoSlider;
