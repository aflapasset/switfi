import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

import starFull from '../assets/icons/star-full.svg';
import starEmpty from '../assets/icons/star-empty.svg';
import starHalf from '../assets/icons/star-half.svg';
import appStore from '../assets/icons/app-store.svg';
import googlePlay from '../assets/icons/google-play.png';

const opinionUrl = 'https://new.api.opinionsystem.fr/v2/company/rating?api_key=40a6a649cb9d8e29e2acecd79893baad';

class ImageHeader extends Component {
	constructor(props) {
		super(props);
		this.state = {
			viewsNb: null,
			rating: null,
			currentBg: null,
			showOpinionsBlock: true,
			showDownloadApp: false,
			stars: []
		};
	}

	static propTypes = {
		location: PropTypes.object.isRequired
	};

	componentDidMount() {
		// Get Opinion System API
		fetch(opinionUrl).then((response) => response.json()).then((data) => this.processData(data.company_rating[0]));

		// Set background depending on location
		switch (this.props.location.pathname) {
			case '/individual':
				this.setState({ currentBg: 'imageHeader--individual' });
				break;
			case '/professional':
				this.setState({
					currentBg: 'imageHeader--professional',
					showOpinionsBlock: false,
					showDownloadApp: true
				});
				break;
			default:
				this.setState({ currentBg: 'imageHeader--home' });
		}
	}

	processData(data) {
		const rating = data.rating;

		this.setState({
			viewsNb: data.survey_total,
			rating: rating
		});

		if (rating < 10) {
			this.setState({ stars: [ 'half', 'empty', 'empty', 'empty', 'empty' ] });
		} else if (rating < 20) {
			this.setState({ stars: [ 'full', 'empty', 'empty', 'empty', 'empty' ] });
		} else if (rating < 30) {
			this.setState({ stars: [ 'full', 'half', 'empty', 'empty', 'empty' ] });
		} else if (rating < 40) {
			this.setState({ stars: [ 'full', 'full', 'empty', 'empty', 'empty' ] });
		} else if (rating < 50) {
			this.setState({ stars: [ 'full', 'full', 'half', 'empty', 'empty' ] });
		} else if (rating < 60) {
			this.setState({ stars: [ 'full', 'full', 'full', 'empty', 'empty' ] });
		} else if (rating < 70) {
			this.setState({ stars: [ 'full', 'full', 'full', 'half', 'empty' ] });
		} else if (rating < 80) {
			this.setState({ stars: [ 'full', 'full', 'full', 'full', 'empty' ] });
		} else if (rating < 90) {
			this.setState({ stars: [ 'full', 'full', 'full', 'full', 'half' ] });
		} else {
			this.setState({ stars: [ 'full', 'full', 'full', 'full', 'full' ] });
		}
	}

	render() {
		const stars = this.state.stars.map((star, index) => {
			return (
				<div key={index}>
					{star === 'empty' ? <img src={starEmpty} alt="étoile vide" className="imageHeader__star" /> : null}
					{star === 'half' ? <img src={starHalf} alt="demis étoile" className="imageHeader__star" /> : null}
					{star === 'full' ? <img src={starFull} alt="étoile pleine" className="imageHeader__star" /> : null}
				</div>
			);
		});

		return (
			<div className={'imageHeader ' + this.state.currentBg}>
				<div className="container imageHeader__container">
					<h1 className="imageHeader__text">{this.props.text}</h1>
					{this.state.showOpinionsBlock ? (
						<div className="d-flex flex-column align-items-center w-100">
							<Link to="/chatbot" className="btn btn__primary mt-5">
								Estimer mon crédit en 5 min
							</Link>
							<div className="mt-auto text-center">
								<div className="mt-4 d-flex align-items-center">
									{stars}
									<p className="imageHeader__views mt-1 ml-1">{this.state.viewsNb} avis vérifiés</p>
								</div>
								<p className="imageHeader__views mt-2">{this.state.rating}% de clients satisfaits</p>
							</div>
						</div>
					) : null}
					{this.state.showDownloadApp ? (
						<div className="imageHeader__appLink">
							<div className="d-flex">
								<a
									style={{ zIndex: '1' }}
									className="mr-1"
									href="https://apps.apple.com/us/app/cr%C3%A9dit-immobilier-switfi/id1330597688"
								>
									<img
										className="imageHeader__appLink__logo"
										alt="Télécharger dans l'App Store"
										src={appStore}
									/>
								</a>
								<a
									style={{ zIndex: '1' }}
									className="ml-1"
									href="https://play.google.com/store/apps/details?id=fr.switfi.android.companion&hl=en_US&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"
								>
									<img
										className="imageHeader__appLink__logo ml-1"
										alt="Disponible sur Google Play"
										src={googlePlay}
									/>
								</a>
							</div>
						</div>
					) : null}
				</div>
			</div>
		);
	}
}

export default withRouter(ImageHeader);
