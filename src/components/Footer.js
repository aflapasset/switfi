import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import switfiLogo from '../assets/logos/switfi-black.svg';
import colorLinesDesktop from '../assets/imgs/color-lines-desktop.svg';
import colorLinesMobile from '../assets/imgs/color-lines-mobile.svg';
import arcDeTriomphe from '../assets/icons/arc-de-triomphe.svg';
import eiffelTower from '../assets/icons/eiffel-tower.svg';
import facebook from '../assets/icons/socials/facebook.svg';
import twitter from '../assets/icons/socials/twitter.svg';
import linkedin from '../assets/icons/socials/linkedin.svg';
import instagram from '../assets/icons/socials/instagram.svg';
import glassdoor from '../assets/icons/socials/glassdoor.svg';
import rgpd from '../assets/logos/rgpd.png';

class Footer extends Component {
	render() {
		return (
			<div className="footer">
				<div className="container-fluid d-flex flex-column flex-lg-row align-items-lg-end justify-content-between">
					<div className="d-flex align-items-end mb-4 mb-lg-0">
						<img src={switfiLogo} alt="Switfi" className="footer__logo" />
						<p className="footer__hint">Vous faciliter le crédit immobilier</p>
					</div>
					<div className="d-flex justify-content-between">
						<Link to="/resources" className="link footer__link">
							Espace presse
						</Link>
						<Link to="/recruitment" className="link footer__link">
							Recrutement
						</Link>
						<Link to="/contact" className="link footer__link">
							Nous contacter
						</Link>
					</div>
				</div>

				<img src={colorLinesDesktop} alt="Color-lines" className="footer__color-lines d-none d-md-block" />
				<img src={colorLinesMobile} alt="Color-lines" className="footer__color-lines d-md-none" />

				<div className="container-fluid footer__info pb-5 pb-lg-4">
					<div className="row">
						<div className="col-12 col-lg-4 mb-4 mb-lg-0 d-flex">
							<div className="d-flex flex-column align-self-end">
								<div className="d-flex mb-2">
									<img src={arcDeTriomphe} alt="Arc de triomphe" className="mr-2" />
									<p className="footer__city">Montpellier</p>
								</div>
								<p className="footer__adress">
									Bât. Le Syracuse, 2 avenue Monteroni d’Arbia<br />
									34920 LE CRES
								</p>
							</div>
						</div>
						<div className="col-12 col-lg-4 mb-5 mb-lg-0">
							<div className="d-flex flex-column">
								<div className="d-flex mb-2">
									<img src={eiffelTower} alt="Tour Eiffel" className="footer__city__icon mr-2" />
									<p className="footer__city">Paris</p>
								</div>
								<p className="footer__adress">
									12 place de la Bastille, Cour Damoye<br />
									75011 PARIS
								</p>
							</div>
						</div>
						<div className="col-12 col-lg-4">
							<div className="d-flex justify-content-center align-items-center h-100">
								<a href="https://twitter.com/switfi" className="footer__social">
									<img src={twitter} alt="twitter" />
								</a>
								<a href="https://www.facebook.com/Switfi/" className="footer__social">
									<img src={facebook} alt="facebook" />
								</a>
								<a href="http://linkedin.com/company/switfi" className="footer__social">
									<img src={linkedin} alt="linkedin" />
								</a>
								<a href="https://www.instagram.com/switfiapp/" className="footer__social">
									<img src={instagram} alt="instagram" />
								</a>
								<a
									href="https://www.glassdoor.fr/Pr%C3%A9sentation/Travailler-chez-Switfi-EI_IE2210190.16,22.htm"
									className="footer__social"
								>
									<img src={glassdoor} alt="glassdoor" />
								</a>
							</div>
						</div>
					</div>
					<div className="row footer__legal">
						<div className="col-12 col-lg-6 mb-4 mb-lg-0">
							<div className="d-flex align-items-center">
								<img src={rgpd} alt="RGPD" className="footer__legal__logo" />
								<p>Switfi protège vos données personnelles</p>
							</div>
						</div>
						<div className="col-12 col-lg-6">
							<div className="d-flex justify-content-center justify-contentl-lg-end align-items-center h-100">
								<span className="footer__legal__copywrights">© E-Owner SAS, 2019.</span>
								<span className="footer__legal__notice">Mentions légales</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Footer;
