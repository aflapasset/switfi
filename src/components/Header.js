import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router';

import signupWhite from '../assets/icons/signup-white.svg';
import signupBlack from '../assets/icons/signup-black.svg';
import switfiLogoWhite from '../assets/logos/switfi-white.svg';
import switfiLogoBlack from '../assets/logos/switfi-black.svg';
import menuWhite from '../assets/icons/menu-white.svg';
import menuBlack from '../assets/icons/menu-black.svg';
import crossWhite from '../assets/icons/cross-white.svg';

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isMenuOpen: false,
			isTransparent: false,
			isSticky: false
		};
		this.toggleMenu = this.toggleMenu.bind(this);
	}

	componentDidMount() {
		window.addEventListener('scroll', () => {
			if (window.pageYOffset > 15) {
				this.setState({ isSticky: true });
			} else {
				this.setState({ isSticky: false });
			}
		});
		this.selectHeaderStyle();
	}

	componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			this.selectHeaderStyle();
			this.setState({ isMenuOpen: false });
		}
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

	selectHeaderStyle() {
		switch (this.props.location.pathname) {
			case '/contact':
				this.setState({ isTransparent: true });
				break;
			case '/chatbot':
				this.setState({ isTransparent: true });
				break;
			case '/appointment':
				this.setState({ isTransparent: true });
				break;
			case '/press':
				this.setState({ isTransparent: true });
				break;
			case '/appointmentValidation':
				this.setState({ isTransparent: true });
				break;
			default:
				this.setState({ isTransparent: false });
		}
	}

	toggleMenu() {
		this.setState(() => ({
			isMenuOpen: !this.state.isMenuOpen
		}));
	}

	render() {
		return (
			<header
				className={`header container-fluid 
			${this.state.isTransparent ? 'header--transparent' : ''}
			${this.state.isSticky ? 'header--sticky' : ''}
			${this.state.isMenuOpen ? 'header--open' : ''}`}
			>
				<div className="header__links">
					<div className="header__left">
						<NavLink to="/">
							{this.state.isTransparent && this.state.isMenuOpen ? (
								<img src={switfiLogoWhite} alt="Switfi" className="header__logo" />
							) : null}
							{!this.state.isTransparent && this.state.isMenuOpen ? (
								<img src={switfiLogoWhite} alt="Switfi" className="header__logo" />
							) : null}
							{this.state.isTransparent && !this.state.isMenuOpen && !this.state.isSticky ? (
								<img src={switfiLogoBlack} alt="Switfi" className="header__logo" />
							) : null}
							{!this.state.isTransparent && !this.state.isMenuOpen && !this.state.isSticky ? (
								<img src={switfiLogoWhite} alt="Switfi" className="header__logo" />
							) : null}
							{this.state.isSticky && !this.state.isMenuOpen ? (
								<img src={switfiLogoBlack} alt="Switfi" className="header__logo" />
							) : null}
						</NavLink>
						<NavLink
							to="/individual"
							className="header__left__link"
							activeClassName="header__left__link--active"
						>
							Particuliers
						</NavLink>
						<NavLink
							to="/professional"
							className="header__left__link"
							activeClassName="header__left__link--active"
						>
							Professionnels
						</NavLink>
						<a href="https://www.switfi.fr/" className="header__left__link">
							Blog
						</a>
						<NavLink
							to="/recruitment"
							className="header__left__link"
							activeClassName="header__left__link--active"
						>
							Rejoindre l’équipe
						</NavLink>
					</div>
					<div className="header__right">
						<a href="https://espace-client.switfi.fr/" className="d-flex align-items-center">
							{this.state.isTransparent && !this.state.isMenuOpen && !this.state.isSticky ? (
								<img src={signupBlack} alt="Signup" className="mr-lg-2" />
							) : null}
							{this.state.isTransparent && this.state.isMenuOpen ? (
								<img src={signupWhite} alt="Signup" className="mr-lg-2" />
							) : null}
							{!this.state.isTransparent && this.state.isMenuOpen ? (
								<img src={signupWhite} alt="Signup" className="mr-lg-2" />
							) : null}
							{!this.state.isTransparent && !this.state.isMenuOpen ? (
								<img src={signupWhite} alt="Signup" className="mr-lg-2" />
							) : null}
							{this.state.isSticky && !this.state.isMenuOpen ? (
								<img src={signupBlack} alt="Signup" className="mr-lg-2" />
							) : null}
							<span className="d-none d-lg-block">Se connecter</span>
						</a>
						<div onClick={this.toggleMenu} className="d-lg-none ml-4">
							{this.state.isTransparent && !this.state.isMenuOpen && !this.state.isSticky ? (
								<img src={menuBlack} alt="" />
							) : null}
							{this.state.isTransparent && this.state.isMenuOpen ? <img src={crossWhite} alt="" /> : null}
							{!this.state.isTransparent && !this.state.isMenuOpen && !this.state.isSticky ? (
								<img src={menuWhite} alt="" />
							) : null}
							{!this.state.isTransparent && this.state.isMenuOpen ? (
								<img src={crossWhite} alt="" />
							) : null}
							{this.state.isSticky && !this.state.isMenuOpen ? <img src={menuBlack} alt="" /> : null}
						</div>
					</div>
				</div>
			</header>
		);
	}
}
export default withRouter(Header);
