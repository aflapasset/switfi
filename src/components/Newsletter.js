import React, { Component } from 'react';
import * as Constants from '../Constants';
import checked from '../assets/icons/checked.svg';

class Newsletter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mailChimpListId: '',
			contactEmail: '',
			formStatus: ''
		};
		this.onFormChange = this.onFormChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	componentDidMount() {
		switch (this.props.page) {
			case 'press':
				this.setState({ mailChimpListId: '98b91b4d15' });
				break;
			case 'individual':
				this.setState({ mailChimpListId: '463c9ebec2' });
				break;
			default:
		}
	}

	onFormChange(event) {
		this.setState({ contactEmail: event.target.value });
	}

	onSubmit(event) {
		event.preventDefault();
		this.setState({ formStatus: 'pending' });

		fetch(
			'https://cors-anywhere.herokuapp.com/' +
				Constants.mailChimpNewsletterUrl +
				this.state.mailChimpListId +
				'/members',
			{
				method: 'POST',
				headers: {
					Authorization: 'Basic ' + Constants.mailChimpAuthorization
				},
				body: JSON.stringify({
					email_address: this.state.contactEmail,
					status: 'subscribed'
				})
			}
		)
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
				this.setState({ formStatus: 'success' });
			});
	}

	renderBtn() {
		switch (this.state.formStatus) {
			case 'pending':
				return (
					<div className="btn btn__secondary btn--line btn--success">
						<div className="lds-ellipsis">
							<div />
							<div />
							<div />
							<div />
						</div>
					</div>
				);
			case 'success':
				return (
					<div className="btn btn__secondary btn--line btn--success">
						<img src={checked} alt="Validé" className="mr-2" />
						Inscription validée
					</div>
				);
			default:
				return <input type="submit" value="M'inscrire" className="btn btn__secondary btn--line" />;
		}
	}

	render() {
		return (
			<div className="container newsletter">
				<h2 className="title title--light mb-4">{this.props.text}</h2>
				<form onSubmit={this.onSubmit}>
					<div className="d-flex flex-column flex-md-row justify-content-center align-items-center">
						<input
							type="email"
							placeholder="E-mail"
							className="input input--line"
							onChange={this.onFormChange}
							required
						/>
						{this.renderBtn()}
					</div>
				</form>
			</div>
		);
	}
}

export default Newsletter;
