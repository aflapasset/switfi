import React, { Component } from 'react';

import arrow from '../assets/icons/arrow.svg';

const opinionUrl = 'https://new.api.opinionsystem.fr/v2/company/rating?api_key=40a6a649cb9d8e29e2acecd79893baad';

class Recommend extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rating: null
		};
	}
	componentDidMount() {
		// Get Opinion System API
		fetch(opinionUrl).then((response) => response.json()).then((data) =>
			this.setState({
				rating: data.company_rating[0].rating
			})
		);
	}

	render() {
		return (
			<div>
				<h2 className="title mb-3">{this.state.rating}% des clients recommandent Switfi</h2>
				<div className="d-flex justify-content-center">
					<a href="http://switfi-le-cres.opinionsystem.fr/" className="link mb-5 text-center">
						Tous les avis vérifiés sur Opinion System
						<img src={arrow} alt="arrow" className="link__arrow" />
					</a>
				</div>
			</div>
		);
	}
}
export default Recommend;
