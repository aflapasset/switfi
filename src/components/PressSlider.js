import React, { Component } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { withRouter } from 'react-router';

import Press from '../PressArticles';

import arrow from '../assets/icons/arrow.svg';
import camera from '../assets/icons/camera.svg';
import video from '../assets/videos/bfm.mp4';
import cross from '../assets/icons/cross-white.svg';

class PressSlider extends Component {
	constructor(props) {
		super(props);
		this.state = {
			press: Press,
			isVideoOpen: false
		};
		this.onToggleVideo = this.onToggleVideo.bind(this);
	}

	componentDidMount() {
		if (this.props.location.pathname === '/individual') {
			this.setState({
				press: Press.filter((article) => article.pages !== 'notIndividual')
			});
		}
	}

	onToggleVideo() {
		this.setState((prevState) => ({
			isVideoOpen: !prevState.isVideoOpen
		}));
	}

	render() {
		const responsive = {
			desktop: {
				breakpoint: { max: 100000, min: 1200 },
				items: 1
			},
			tablet: {
				breakpoint: { max: 1200, min: 768 },
				items: 1
			},
			mobile: {
				breakpoint: { max: 768, min: 0 },
				items: 1
			}
		};

		const pressLogos = require.context('../assets/logos/pressSlider', true);

		const pressList = this.state.press.map((item, index) => {
			let logoSrc = pressLogos(`./${item.logo}.png`);
			return (
				<div className="pressSlider__item" key={index}>
					<div className="pressSlider__logo" style={{ backgroundImage: `url(${logoSrc})` }} />
					<div className="d-flex flex-column">
						<div className="d-flex">
							<p className="pressSlider__title">{item.title}</p>
							{item.link ? null : (
								<div className="pressSlider__camera">
									<img src={camera} alt="Presse" />
								</div>
							)}
						</div>
						<p className="pressSlider__text">{item.text}</p>
						<div className="d-flex mt-3">
							{item.link ? (
								<a
									href={item.link}
									target="_blank"
									rel="noopener noreferrer"
									className="link text-left"
								>
									Voir l'article
									<img src={arrow} alt="arrow" className="link__arrow" />
								</a>
							) : (
								<button className="link text-left" onClick={this.onToggleVideo}>
									Voir la vidéo
									<img src={arrow} alt="arrow" className="link__arrow" />
								</button>
							)}
						</div>
					</div>
				</div>
			);
		});

		return (
			<div>
				{this.props.title ? (
					<h2 className="title">{this.props.title}</h2>
				) : (
					<h2 className="title">La presse en parle</h2>
				)}

				<Carousel
					responsive={responsive}
					additionalTransfrom={0}
					arrows={false}
					autoPlay={false}
					autoPlaySpeed={6000}
					centerMode={false}
					draggable
					focusOnSelect={false}
					infinite
					showDots={false}
					slidesToSlide={1}
					swipeable
				>
					{pressList}
				</Carousel>
				{this.state.isVideoOpen ? (
					<div className="mediaFullScreen">
						<div className="mediaFullScreen__container fadeIn1s">
							<video controls src={video} autoPlay type="video/mp4" />
							<img
								src={cross}
								alt="fermer"
								className="mediaFullScreen__close"
								onClick={this.onToggleVideo}
							/>
						</div>
					</div>
				) : null}
			</div>
		);
	}
}

export default withRouter(PressSlider);
