const press = [
	{
		pages: 'all',
		title: 'BFM',
		logo: 'BFM',
		text:
			"La vie immo : L'application Switfi propose de réduire à quinze minutes l'audit financier d'un dossier de crédit.",
		link: ''
	},
	{
		pages: 'all',
		title: "le journal de l'agence",
		logo: 'le-journal-de-l-agence',
		text: 'Switfi devient partenaire de la Fnaim Grand Paris.',
		link: 'https://www.journaldelagence.com/1156540-switfi-devient-partenaire-de-la-fnaim-grand-paris'
	},
	{
		pages: 'notIndividual',
		title: 'radio immo',
		logo: 'radio-immo',
		text:
			"Switfi est une application mobile de courtage bancaire destinée aux professionnels de l'immobilier pour leur permettre de rassurer leurs clients le plus rapidement possible sur la faisabilité de leur achat.",
		link: 'https://radio.immo/broadcast/160628-Yann-NICODEME-SWITFI'
	},
	{
		pages: 'notIndividual',
		title: 'immomatin',
		logo: 'immomatin',
		text:
			'Sécuriser un projet immobilier en vérifiant, en 15 minutes, la capacité d’emprunt d’un acquéreur : telle est la promesse que fait Switfi aux professionnels de l’immobilier.',
		link:
			'https://www.immomatin.com/articles/logiciels-d-evaluati/switfi-verifie-les-capacites-d-emprunt-des-acquereurs.htm'
	},
	{
		pages: 'notIndividual',
		title: 'immobilier 2.0',
		logo: 'immobilier-2.0',
		text:
			"L'application Switfi permet aux agents de vérifier et de faciliter l'accès au crédit pour les acheteurs potentiels.",
		link: 'https://immo2.pro/annuaire/prestataire/credit-immobilier/switfi/'
	},
	{
		pages: 'notIndividual',
		title: "le journal de l'agence",
		logo: 'le-journal-de-l-agence',
		text: "Switfi révolutionne le taux de transformation des professionnels de l'immobilier.",
		link:
			'https://www.journaldelagence.com/1155906-switfi-revolutionne-le-taux-de-transformation-des-professionnels-de-limmobilier'
	},
	{
		pages: 'all',
		title: 'pap',
		logo: 'pap',
		text:
			'Cette application mobile permet de vérifier en un temps record, 15 minutes, la faisabilité financière de votre projet.',
		link: 'https://www.pap.fr/actualites/cinq-applications-special-immobilier/a20383'
	},
	{
		pages: 'all',
		title: 'le moniteur',
		logo: 'le-moniteur',
		text: 'Et si la start-up Switfi révolutionnait le courtage immobilier ?',
		link:
			'https://www.lemoniteur.fr/article/rent-2018-et-si-la-start-up-switfi-revolutionnait-le-courtage-immobilier.1995739'
	},
	{
		pages: 'notIndividual',
		title: 'presse agence',
		logo: 'presse-agence',
		text: "La Fnaim Grand Paris signe un partenariat avec l'application Switfi.",
		link:
			'http://www.presseagence.fr/lettre-economique-politique-paca/2018/07/14/paris-switfi-revolutionne-le-taux-de-transformation-des-professionnels-de-limmobilier-grace-a-son-application/'
	},
	{
		pages: 'all',
		title: 'le journal du grand paris',
		logo: 'le-journal-du-grand-paris',
		text: 'Switfi transforme l’horizon des professionnels de l’immobilier.',
		link: 'https://www.lejournaldugrandparis.fr/fnaim-grand-paris-e-owner-signent-partenariat-lapplication-swifti/'
	},
	{
		pages: 'notIndividual',
		title: 'herault juridique & economique',
		logo: 'herault-juridique-&-economique',
		text:
			'La start-up montpelliéraine lance une application mettant au service des acteurs de l’immobilier des moyens techniques et humains afin de vérifier et faciliter l’accès au crédit de leurs clients… en 15 minutes !',
		link:
			'https://heraultjuridique.com/immobilier/switfi-veut-revolutionner-le-taux-de-transformation-des-professionnels-de-limmobilier/'
	},
	{
		pages: 'notIndividual',
		title: 'metropolitain',
		logo: 'metropolitain',
		text:
			'"Pour nous, cela signifie plus de flexibilité et de réactivité et une mutation assumée vers le numérique" explique Michel PLATERO, Président de la Fnaim Grand Paris.',
		link:
			'https://actu.fr/economie/les-news-de-la-french-tech-montpellier-netacheteur-my-horse-familly-et-switfi_23147551.html'
	}
];

export default press;
