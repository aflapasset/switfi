import React, { Component } from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import ImageHeader from '../components/ImageHeader';
import LogosSlider from '../components/LogosSlider';
import OpinionsSlider from '../components/OpinionsSlider';
import PressSlider from '../components/PressSlider';
import PhoneSlider from '../components/PhoneSlider';

import house from '../assets/icons/house.svg';
import sheets from '../assets/icons/sheets.svg';
import steps from '../assets/icons/steps.svg';
import checked from '../assets/icons/checked.svg';

import eliza from '../assets/imgs/professionals/eliza.png';
import jim from '../assets/imgs/professionals/jim.png';
import samuel from '../assets/imgs/professionals/samuel.png';
import yann from '../assets/imgs/professionals/yann.png';

import * as Constants from '../Constants';

class Professional extends Component {
	constructor(props) {
		super(props);
		this.state = {
			phoneNumbers: '',
			formSuccess: false
		};
		this.onFormChange = this.onFormChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onFormChange(event) {
		this.setState({ phoneNumbers: event.target.value });
	}

	onSubmit(event) {
		event.preventDefault();

		console.log(this.state.phoneNumbers);
		fetch('https://cors-anywhere.herokuapp.com/' + Constants.smsPartnerUrl, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				apiKey: 'd936802fc8059812cfbbd00527b07dbc4164388c',
				phoneNumbers: this.state.phoneNumbers,
				message: "Voici le lien de téléchargement de l'application Switfi: http://onelink.to/cmrcvm",
				sender: 'Switfi'
			})
		})
			.then((response) => response.json())
			.then((data) => {
				this.setState({ formSuccess: true });
				console.log(data);
			});
	}

	render() {
		return (
			<div className="professional">
				<ImageHeader text="Optimisez vos ventes et donnez le sourire à vos clients avec l’application gratuite Switfi" />
				<div className="container">
					<div className="professional__sms">
						<a href="http://onelink.to/cmrcvm" className="btn btn__primary d-lg-none">
							Télécharger l'application gratuite
						</a>
						<p className="imageHeader__appLink__text">
							Saisissez votre numéro de mobile pour recevoir le lien de téléchargement
						</p>
						<form onSubmit={this.onSubmit}>
							<div className="d-flex flex-column flex-md-row justify-content-center">
								<input
									type="text"
									placeholder="N° tél. mobile"
									className="input input--line imageHeader__appLink__input"
									onChange={this.onFormChange}
									required
								/>
								{this.state.formSuccess ? (
									<div className="btn btn__secondary btn--line btn--success">
										<img src={checked} alt="Validé" className="mr-2" />
										SMS envoyé
									</div>
								) : (
									<input
										type="submit"
										value="M'envoyer le lien par SMS"
										className="btn btn__primary btn--line imageHeader__appLink__btn"
									/>
								)}
							</div>
						</form>
					</div>
					<h2 className="title professional__title">Comment ça fonctionne ?</h2>

					{/* STEP 1 */}
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block block--step d-flex">
							<div className="block--step__step">
								<div className="block--step__step__number">1</div>
								<img src={house} alt="Jauge" />
							</div>
							<div>
								<h3 className="block--step__title">
									Mon prospect évalue gratuitement sa capacité d’emprunt au cours de notre premier
									rendez-vous
								</h3>
								<p className="block--step__text">
									Quelques questions suffisent pour lui estimer sa réelle capacité d’emprunt. Je peux
									alors lui proposer le bien qui lui convient.
								</p>
							</div>
						</div>
					</ScrollAnimation>

					{/* STEP 2 */}
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block block--step d-flex">
							<div className="block--step__step">
								<div className="block--step__step__number">2</div>
								<img src={sheets} alt="Jauge" />
							</div>
							<div>
								<h3 className="block--step__title">Les justificatifs sont téléchargeables en ligne</h3>
								<p className="block--step__text">
									Dès l’estimation réalisée par l’application Switfi, mon client partage facilement
									ses justificatifs via l’espace dédié et sécurisé. Constituer son offre de prêt est
									donc un jeu d’enfant.
								</p>
							</div>
						</div>
					</ScrollAnimation>

					{/* STEP 3 */}
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block block--step d-flex">
							<div className="block--step__step">
								<div className="block--step__step__number">3</div>
								<img src={steps} alt="Jauge" />
							</div>
							<div>
								<h3 className="block--step__title">Je suis l’avancée de mes dossiers</h3>
								<p className="block--step__text">
									Depuis mon smartphone ou ordinateur, je suis notifié en temps réel de l’état
									d’avancement de mes ventes.
								</p>
							</div>
						</div>
					</ScrollAnimation>

					<div className="professional__questions">
						<h2 className="title">Des questions ? Envie d’une démonstration ?</h2>
						<div className="professional__advisors">
							<div className="professional__advisors__img">
								<img src={yann} alt="" />
							</div>
							<div className="professional__advisors__img">
								<img src={samuel} alt="" />
							</div>
							<div className="professional__advisors__img">
								<img src={eliza} alt="" />
							</div>
							<div className="professional__advisors__img">
								<img src={jim} alt="" />
							</div>
						</div>
						<a
							href="mailto:commercial@switfi.fr?subject=Contacter%20le%20service%20commercial"
							className="btn btn__secondary mt-4"
						>
							Prendre contact avec un conseiller Switfi
						</a>
					</div>
				</div>
				<PhoneSlider />
				<div className="container">
					<h2 className="title">Les professionnels de l’immobilier recommandent Switfi</h2>
				</div>

				<OpinionsSlider />

				<section className="home__partners">
					<div className="home__partners__bg">
						<div className="home__partners__bg__confetti" />
					</div>
					<div className="home__partners__container">
						<h2 className="title mb-5">
							Nous négocions les meilleures conditions pour vos clients avec nos partenaires
						</h2>

						<LogosSlider logosNb={8} />
					</div>
				</section>

				<PressSlider />
			</div>
		);
	}
}

export default Professional;
