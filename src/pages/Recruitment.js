import React, { Component } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import RecruitmentBlock from '../components/RecruitmentBlock';
import PressSlider from '../components/PressSlider';
import ScrollAnimation from 'react-animate-on-scroll';

import carouselImg1 from '../assets/imgs/recruitment/carousel/carousel_1.jpg';
import carouselImg2 from '../assets/imgs/recruitment/carousel/carousel_2.jpg';
import carouselImg3 from '../assets/imgs/recruitment/carousel/carousel_3.jpg';
import carouselImg4 from '../assets/imgs/recruitment/carousel/carousel_4.jpg';
import carouselImg5 from '../assets/imgs/recruitment/carousel/carousel_5.jpg';

import female from '../assets/icons/female.svg';
import male from '../assets/icons/male.svg';
import trust from '../assets/icons/trust.svg';
import love from '../assets/icons/love.svg';
import muscles from '../assets/icons/muscles.svg';
import smile from '../assets/icons/smile.svg';
import play from '../assets/icons/play.svg';
import glassdoor from '../assets/logos/glassdoor.png';
import arrowWhite from '../assets/icons/arrow-white.svg';
import step1 from '../assets/imgs/recruitment/process/step1.png';
import step2 from '../assets/imgs/recruitment/process/step2.png';
import step3 from '../assets/imgs/recruitment/process/step3.png';
import cross from '../assets/icons/cross-white.svg';
import videoGaelle from '../assets/videos/interviewGaelle.mp4';
import videoJim from '../assets/videos/interviewJim.mp4';
import videoLea from '../assets/videos/interviewLea.mp4';
import playYellow from '../assets/icons/play-yellow.svg';
import zoomYellow from '../assets/icons/zoom.svg';

class Recruitment extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isModalOpen: false,
			mediaType: '',
			mediaToDisplay: null
		};
		this.onToggleModal = this.onToggleModal.bind(this);
	}

	onToggleModal(media, type) {
		this.setState((prevState) => ({
			isModalOpen: !prevState.isModalOpen,
			mediaType: type,
			mediaToDisplay: media
		}));
	}

	render() {
		const images = require.context('../assets/imgs/recruitment/blocks', true);
		let waitingRoom = images('./waiting-room.png');
		let lea = images('./lea.png');
		let jim = images('./jim.png');
		let office = images('./office.png');
		let beach = images('./beach.png');
		let meeting = images('./meeting.png');
		let gaelle = images('./gaelle.png');

		const responsive = {
			desktop: {
				breakpoint: { max: 100000, min: 1200 },
				items: 1
			},
			tablet: {
				breakpoint: { max: 1200, min: 768 },
				items: 1
			},
			mobile: {
				breakpoint: { max: 768, min: 0 },
				items: 1
			}
		};

		return (
			<div className="recruitment">
				<Carousel
					responsive={responsive}
					itemClass="recruitment__carousel__item"
					dotListClass="recruitment__carousel__dot"
					additionalTransfrom={0}
					arrows={false}
					autoPlay
					autoPlaySpeed={3000}
					centerMode={false}
					draggable
					focusOnSelect={false}
					infinite
					keyBoardControl
					minimumTouchDrag={80}
					renderDotsOutside={false}
					showDots
					slidesToSlide={1}
					swipeable
				>
					<div style={{ backgroundImage: `url(${carouselImg1})` }} />
					<div style={{ backgroundImage: `url(${carouselImg2})` }} />
					<div style={{ backgroundImage: `url(${carouselImg3})` }} />
					<div style={{ backgroundImage: `url(${carouselImg4})` }} />
					<div style={{ backgroundImage: `url(${carouselImg5})` }} />
				</Carousel>

				<div className="container">
					<div className="block recruitment__firstBlock fadeIn1s">
						<h2 className="title title--lg">Rejoindre Switfi</h2>
						<div className="row">
							<div className="col-12 col-lg-6  order-lg-12">
								<div className="d-flex justify-content-around">
									<div className="recruitment__firstBlock__data yellow">
										<div className="recruitment__firstBlock__year">2016</div>
										<div className="">année de création</div>
									</div>
									<div className="recruitment__firstBlock__data green">
										<div className="recruitment__firstBlock__year">13</div>
										<div className="">collaborateurs</div>
									</div>
								</div>
								<div className="d-flex justify-content-around">
									<div className="recruitment__firstBlock__data blue">
										<div className="recruitment__firstBlock__year d-flex justify-content-center align-items-center">
											30%
											<img src={male} alt="Homme" className="ml-1 mr-3" />
											70%
											<img src={female} alt="Femme" className="ml-1" />
										</div>
										<div className="">parité</div>
									</div>
									<div className="recruitment__firstBlock__data pink">
										<div className="recruitment__firstBlock__year">36 ans</div>
										<div className="">âge moyen</div>
									</div>
								</div>
							</div>
							<div className="col-12 col-lg-6 mb-3 mb-lg-0 text-justify">
								Switfi évolue tous les jours. <br /> Saisissez l’opportunité et venez développer cette
								formidable entreprise avec nous&nbsp;! <br /> Plus qu’un job, ce sont des valeurs et un
								état d’esprit que nous vous proposons. Une expérience professionnelle unique où vous
								pourrez exprimer votre talent autant que vous le souhaitez… <br /> Avec de l’humour, de
								la bonne humeur et des viennoiseries, votre intégration se fera très facilement. Chez
								Switfi nous avons l’habitude de fêter tout ce que nous pouvons. Venez avec vos idées et
								votre envie, on s’occupe des chouquettes&nbsp;!
							</div>
						</div>
						<h2 className="title text-left mt-4">Nos valeurs chez Switfi</h2>
						<div className="row">
							<div className="col-6 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center align-items-end">
								<div className="text-center">
									<img src={trust} alt="" />
									<p className="recruitment__firstBlock__values">Confiance</p>
								</div>
							</div>
							<div className="col-6 col-lg-3 mb-4 mb-lg-0 d-flex justify-content-center align-items-end">
								<div className="text-center">
									<img src={love} alt="" />
									<p className="recruitment__firstBlock__values">Partage</p>
								</div>
							</div>
							<div className="col-6 col-lg-3 d-flex justify-content-center align-items-end">
								<div className="text-center">
									<img src={muscles} alt="" />
									<p className="recruitment__firstBlock__values">Sans cesse progresser</p>
								</div>
							</div>
							<div className="col-6 col-lg-3 d-flex justify-content-center align-items-end">
								<div className="text-center">
									<img src={smile} alt="" />
									<p className="recruitment__firstBlock__values">Good mood</p>
								</div>
							</div>
						</div>
					</div>

					<h2 className="title mt-3 mb-40">Pourquoi rejoindre Switfi ?</h2>
					<div className="recruitment__blocks-container">
						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={400} animateOnce>
								<RecruitmentBlock
									name="samuel"
									position="Responsable commercial IDF"
									text="J’ai rejoint Switfi pour son côté novateur et unique, pour sa dimension humaine et pour l’intérêt que porte les dirigeants à mettre leur équipe au coeur de chaque projet."
								/>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--img"
									style={{ backgroundImage: `url(${waitingRoom})` }}
									onClick={() => this.onToggleModal(waitingRoom)}
								>
									<div className="recruitmentBlock--img__hover">
										<img src={zoomYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={800} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--video"
									style={{ backgroundImage: `url(${lea})` }}
									onClick={() => this.onToggleModal(videoLea, 'video')}
								>
									<div className="recruitmentBlock--video__info">
										<div className="d-flex align-items-center">
											<img src={play} alt="Lecture" />
										</div>
										<div className="d-flex flex-column ml-3">
											<p className="recruitmentBlock--video__name">Léa</p>
											<p>Conseillère en financement</p>
										</div>
									</div>
									<div className="recruitmentBlock--video__hover">
										<img src={playYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={400} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--video"
									style={{ backgroundImage: `url(${jim})` }}
									onClick={() => this.onToggleModal(videoJim, 'video')}
								>
									<div className="recruitmentBlock--video__info">
										<div className="d-flex align-items-center">
											<img src={play} alt="Lecture" />
										</div>
										<div className="d-flex flex-column ml-3">
											<p className="recruitmentBlock--video__name">Jim</p>
											<p>Responsable commercial</p>
										</div>
									</div>
									<div className="recruitmentBlock--video__hover">
										<img src={playYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={800} animateOnce>
								<RecruitmentBlock
									name="julien"
									position="CTO"
									text="En charge de l’équipe de développement de Switfi depuis ses débuts, c’est pour moi une belle aventure humaine pleine de challenges techniques avec l’ambition de révolutionner l’accessibilité aux financements de nos clients."
								/>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
								<RecruitmentBlock
									name="sandrine"
									position="Responsable pôle Courtage"
									text="C’est pour sa différence, son côté novateur, et son dynamismeque j’ai rejoint l’équipe Switfi. Je n’ai aucun regret d’avoir quitté un réseau de franchisés… Faites comme moi et sautez le pas !"
								/>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={400} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--img"
									style={{ backgroundImage: `url(${office})` }}
									onClick={() => this.onToggleModal(office)}
								>
									<div className="recruitmentBlock--img__hover">
										<img src={zoomYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--img"
									style={{ backgroundImage: `url(${beach})` }}
									onClick={() => this.onToggleModal(beach)}
								>
									<div className="recruitmentBlock--img__hover">
										<img src={zoomYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={800} animateOnce>
								<a
									href="https://www.glassdoor.fr/Pr%C3%A9sentation/Travailler-chez-Switfi-EI_IE2210190.16,22.htm"
									className="block recruitmentBlock recruitmentBlock--glassdoor"
								>
									<img
										src={glassdoor}
										alt="Glassdoor"
										className="recruitmentBlock--glassdoor__logo"
									/>
									<div className="recruitmentBlock--glassdoor__footer">
										<p className="mr-3 recruitmentBlock--glassdoor__text">
											Voir les avis des employés
										</p>
										<img src={arrowWhite} alt="Flèche" />
									</div>
								</a>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={400} animateOnce>
								<RecruitmentBlock
									name="laura"
									position="Product Owner"
									text="J’ai démarré chez Switfi en alternance, première recrue de la société, une grande fierté ! Avec le temps, une équipe de choc s’est construite, il ne manque plus que vous !"
								/>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
								<RecruitmentBlock
									name="yann"
									position="CEO"
									text="Switfi bouscule les façons de faire du courtage immobilier, et c’est grâce à nos différences que nous satisfaisons clients et partenaires. Vous partagez les valeurs de Switfi, vous avez envie de bouger les lignes ? Rejoignez l’aventure !"
								/>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} delay={800} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--img"
									style={{ backgroundImage: `url(${meeting})` }}
									onClick={() => this.onToggleModal(meeting)}
								>
									<div className="recruitmentBlock--img__hover">
										<img src={zoomYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>

						<div className="recruitmentBlock__container">
							<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
								<div
									className="block recruitmentBlock recruitmentBlock--video"
									style={{ backgroundImage: `url(${gaelle})` }}
									onClick={() => this.onToggleModal(videoGaelle, 'video')}
								>
									<div className="recruitmentBlock--video__info">
										<div className="d-flex align-items-center">
											<img src={play} alt="Lecture" />
										</div>
										<div className="d-flex flex-column ml-3">
											<p className="recruitmentBlock--video__name">Gaëlle</p>
											<p>Office Manager</p>
										</div>
									</div>
									<div className="recruitmentBlock--video__hover">
										<img src={playYellow} alt="Lecture" />
									</div>
								</div>
							</ScrollAnimation>
						</div>
					</div>

					{/* NOS OFFRES */}
					<h2 className="title">Nos offres</h2>
					<div className="d-flex justify-content-center flex-wrap">
						<div className="block recruitment__offer p-4 m-2">
							<p className="bold mb-1">Analyste courtier sédentaire (H/F)</p>
							<p className="roboto">CDI</p>
							<p className="roboto mb-5">Toute la France</p>
							<a
								href="mailto:contact@switfi.fr?subject=Contacter%20le%20service%20recrutement"
								className="btn btn__primary m-auto"
							>
								Postuler
							</a>
						</div>
						<div className="block recruitment__offer p-4 m-2">
							<p className="bold mb-1">Commercial BtoB</p>
							<p className="roboto">Mandataire</p>
							<p className="roboto mb-5">Paris, France</p>
							<a
								href="mailto:contact@switfi.fr?subject=Contacter%20le%20service%20recrutement"
								className="btn btn__primary m-auto"
							>
								Postuler
							</a>
						</div>
					</div>

					{/* Notre process de recrutement */}
					<h2 className="title mt-100">Notre process de recrutement</h2>
					<div className="recruitment__process">
						<div className="d-flex flex-column flex-lg-row align-items-center justify-content-center">
							<img src={step1} alt="" className="recruitment__process__img" />
							<p className="recruitment__process__text d-lg-none">
								Entretien téléphonique avec Gaëlle, Office Manager
							</p>
							<div className="recruitment__process__dots">
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot d-none d-lg-block" />
								<span className="recruitment__process__dot d-none d-lg-block" />
								<span className="recruitment__process__dot d-none d-lg-block" />
							</div>
							<img src={step2} alt="" className="recruitment__process__img" />
							<p className="recruitment__process__text d-lg-none">
								Entretien avec l’équipe concernée dans les locaux de Switfi
							</p>
							<div className="recruitment__process__dots">
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot" />
								<span className="recruitment__process__dot d-none d-lg-block" />
								<span className="recruitment__process__dot d-none d-lg-block" />
								<span className="recruitment__process__dot d-none d-lg-block" />
							</div>
							<img src={step3} alt="" className="recruitment__process__img" />
							<p className="recruitment__process__text d-lg-none">Repas d’intégration avec l’équipe</p>
						</div>
						<div className="recruitment__process__text-container">
							<p className="recruitment__process__text">
								Entretien téléphonique avec Gaëlle, Office Manager
							</p>
							<p className="recruitment__process__text">
								Entretien avec l’équipe concernée dans les locaux de Switfi
							</p>
							<p className="recruitment__process__text">Repas d’intégration avec l’équipe</p>
						</div>
					</div>
				</div>
				{/* La presse en parle */}
				<PressSlider newsletter={false} />

				{this.state.isModalOpen ? (
					<div className="mediaFullScreen">
						<div className="mediaFullScreen__container fadeIn1s">
							{this.state.mediaType === 'video' ? (
								<video controls src={this.state.mediaToDisplay} autoPlay type="video/mp4" />
							) : (
								<img src={this.state.mediaToDisplay} alt={this.state.mediaToDisplay} />
							)}
							<img
								src={cross}
								alt="fermer"
								className="mediaFullScreen__close"
								onClick={this.onToggleModal}
							/>
						</div>
					</div>
				) : null}
			</div>
		);
	}
}

export default Recruitment;
