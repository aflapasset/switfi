import React, { Component } from 'react';

class Chatbot extends Component {
	render() {
		return (
			<div>
				<iframe
					className="fullpage"
					style={{ border: 'none' }}
					title="Chatbot"
					src="https://app.ideta.io/web-display/-LhZIV4sy9JcaJh0RjBF"
				/>
			</div>
		);
	}
}

export default Chatbot;
