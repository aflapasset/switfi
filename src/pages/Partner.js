import React, { Component } from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import LogosSlider from '../components/LogosSlider';
import OpinionsSlider from '../components/OpinionsSlider';
import Recommend from '../components/Recommend';
import PressSlider from '../components/PressSlider';

import rocket from '../assets/icons/rocket.svg';
import team from '../assets/icons/team.svg';
import checklist from '../assets/icons/checklist.svg';
import scan from '../assets/icons/scan.svg';
import connection from '../assets/icons/connection.svg';
import yourLogo from '../assets/icons/your-logo.svg';
import yann from '../assets/imgs/yann.jpg';

class Partner extends Component {
	render() {
		return (
			<div className="partner">
				<div className="partner__header">
					<h1 className="headerImgTitle">
						Technologie et expertise,<br /> ce qui fait de Switfi votre partenaire idéal
					</h1>
				</div>
				<div className="container">
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block partner__firstBlock">
							<h3 className="title">
								Le financement immobilier,<br className="d-none d-lg-block" /> un processus long et
								rigoureux pour vos équipes ?
							</h3>
							<p className="partner__firstBlock__text">
								Créée dans le but de réaliser de vrais partenariats gagnants/gagnants, Switfi a pour
								ambition de ne vous apporter que des clients qualifiés. Vous gagnez du temps, nous
								gagnons en satisfaction clients.
							</p>
							<div className="d-flex justify-content-around">
								<div className="partner__firstBlock__data">
									<img src={rocket} alt="Fusée" />
									<p className="partner__firstBlock__data__number">2016</p>
									<p>année de création</p>
								</div>
								<div className="partner__firstBlock__data">
									<img src={team} alt="Fusée" />
									<p className="partner__firstBlock__data__number">13</p>
									<p>collaborateurs</p>
								</div>
							</div>
						</div>
					</ScrollAnimation>

					{/* Comment ça fonctionne ? */}
					<h2 className="title">Comment ça fonctionne ?</h2>
					<div className="block block--step d-flex">
						<div className="block--step__step">
							<div className="block--step__step__number">1</div>
							<img src={checklist} alt="Jauge" />
						</div>
						<div>
							<h3 className="block--step__title">
								Nous qualifions efficacement les demandes de crédit en 10 minutes grâce à notre process
								entièrement digitalisé
							</h3>
							<p className="block--step__text">
								L’application mise à disposition des professionnels de l’immobilier va analyser la
								faisabilité économique du projet mais aussi le profil des emprunteurs selon vos critères
								d’acceptation.
							</p>
						</div>
					</div>
					<div className="block block--step d-flex">
						<div className="block--step__step">
							<div className="block--step__step__number">2</div>
							<img src={scan} alt="Jauge" />
						</div>
						<div>
							<h3 className="block--step__title">
								Nous authentifions les pièces justificatives et labellisons la complétude du dossier
								pour plus de sécurité
							</h3>
							<p className="block--step__text">
								Nos conseillers, aidés des algorithmes Switfi, authentifiient et valident la complétude
								du dossier constitué sur un espace sécurisé.
							</p>
						</div>
					</div>
					<div className="block block--step d-flex">
						<div className="block--step__step">
							<div className="block--step__step__number">3</div>
							<img src={connection} alt="Jauge" />
						</div>
						<div>
							<h3 className="block--step__title">
								Nous vous présentons des dossiers qui répondent à vos critères de façon simple en nous
								connectant à vos outils
							</h3>
							<p className="block--step__text">
								L’expertise Switfi et les outils numériques développés permettent d’obtenir la
								satisfaction des clients ainsi que celle de vos équipes.
							</p>
						</div>
					</div>
				</div>
				<div className="loopBg" />
				<div className="partner__partners">
					<div className="container">
						<h2 className="title">
							Nous collaborons déjà avec plusieurs partenaires bancaires reconnus, pourquoi pas vous ?
						</h2>
					</div>
					<LogosSlider logosNb={8} />
					<div className="container">
						<div className="logos-slider__item partner__yourLogo">
							<div className="partner__yourLogo__container">
								<img src={yourLogo} alt="Banque" />
								Votre logo ?
							</div>
						</div>

						<div className="block block--contact">
							<img src={yann} alt="Yann Nicodeme" className="block--contact__img d-none d-md-block" />
							<div className="block--contact__info">
								<h2 className="title mb-4 mb-md-0 w-100">
									Votre interlocuteur privilégié pour plus d’informations
								</h2>
								<div className="d-flex w-100 align-items-center justify-content-center">
									<img
										src={yann}
										alt="Yann Nicodeme"
										className="block--contact__img mr-3 d-md-none"
									/>
									<div className="block--contact__text">
										<p className="block--contact__name">Yann NICODEME</p>
										<p className="block--contact__position">CEO, Switfi</p>
									</div>
								</div>
								<a
									href="mailto:contact@switfi.fr?subject=Contacter%20Yann,%20responsable%20partenariats%20bancaires"
									className="btn btn__primary press__laura-btn"
								>
									Contacter Yann
								</a>
							</div>
						</div>
						<Recommend />
					</div>

					<OpinionsSlider />

					<PressSlider />
				</div>
			</div>
		);
	}
}

export default Partner;
