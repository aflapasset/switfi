import React, { Component } from 'react';

import phone from '../assets/icons/phone.svg';
import arcDeTriomphe from '../assets/icons/arc-de-triomphe.svg';
import eiffelTower from '../assets/icons/eiffel-tower.svg';
import checked from '../assets/icons/checked.svg';

class Contact extends Component {
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.state = {
			formStatus: ''
		};
	}

	handleSubmit(e) {
		e.preventDefault();
		const firstName = document.getElementById('firstName').value;
		const lastName = document.getElementById('lastName').value;
		const email = document.getElementById('email').value;
		const phone = document.getElementById('phone').value;
		const message = document.getElementById('message').value;

		fetch('/send', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				phone: phone,
				message: message
			})
		})
			.then((response) => response.json())
			.then((data) => {
				console.log(data);
				this.setState({ formStatus: 'success' });
				document.getElementById('contact-form').reset();
			});
	}

	render() {
		return (
			<div className="contact">
				<div className="container">
					<h1 className="title">Une équipe de professionnels à votre écoute</h1>
					<div className="contact__headerImg" />

					<div className="contact__content">
						<div className="contact__phone-container">
							<h2 className="title contact__title contact__title--phone f-50">
								Une question urgente ? <br /> Contactez-nous par téléphone
							</h2>
							<div className="f-50">
								<a href="tel:0411950751">
									<div className="contact__phone">
										<div className="d-flex align-items-center">
											<img className="contact__phone__icon" src={phone} alt="" />
										</div>
										<div className="contact__phone__number">04 11 95 07 51</div>
									</div>
								</a>
							</div>
						</div>

						<h2 className="title contact__title">Envie de passer boire un café ?</h2>
						<div className="contact__adress">
							<div className="f-50 align-self-end">
								<div className="d-flex mb-2">
									<img src={arcDeTriomphe} alt="Arc de triomphe" className="mr-2" />
									<p className="footer__city">Montpellier</p>
								</div>
								<p className="footer__adress">
									Bât. Le Syracuse, 2 avenue Monteroni d’Arbia<br />
									34920 LE CRES
								</p>
							</div>
							<div className="f-50">
								<div className="d-flex mb-2">
									<img src={eiffelTower} alt="Tour Eiffel" className="footer__city__icon mr-2" />
									<p className="footer__city">Paris</p>
								</div>
								<p className="footer__adress">
									12 place de la Bastille, Cour Damoye<br />
									75011 PARIS
								</p>
							</div>
						</div>

						{/* FORM */}
						<h2 className="title contact__title mb-0">Envoyer un message à l’équipe</h2>
						<form className="contact__form" id="contact-form" onSubmit={this.handleSubmit}>
							<div className="d-flex flex-wrap">
								<div className="f-50">
									<label className="label">Prénom</label>
									<input
										className="input form__input"
										type="text"
										placeholder="Chew"
										id="firstName"
										required
									/>
								</div>
								<div className="f-50">
									<label className="label">NOM</label>
									<input
										className="input form__input"
										type="text"
										placeholder="BACCA"
										id="lastName"
										required
									/>
								</div>
							</div>
							<div className="d-flex flex-wrap">
								<div className="f-50">
									<label className="label">Email</label>
									<input
										className="input form__input"
										type="email"
										placeholder="chew.bacca@wookie.com"
										id="email"
										required
									/>
								</div>
								<div className="f-50">
									<label className="label">Téléphone</label>
									<input
										className="input form__input form__input--phone"
										type="tel"
										placeholder="06 19 10 19 77"
										id="phone"
										required
									/>
								</div>
							</div>
							<label className="label">Message</label>
							<textarea className="input form__textarea" id="message" required />

							{this.state.formStatus === '' ? (
								<input className="btn btn__primary m-auto" type="submit" value="Envoyer mon message" />
							) : null}
							{this.state.formStatus === 'success' ? (
								<div className="d-flex justify-content-center">
									<div className="btn btn__secondary btn--success">
										<img src={checked} alt="Validé" className="mr-2" />
										Message envoyé
									</div>
								</div>
							) : null}
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default Contact;
