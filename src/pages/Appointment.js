import React, { Component } from 'react';
import Moment from 'react-moment';
import 'moment/locale/fr';

import arrow from '../assets/icons/arrow.svg';

import * as Constants from '../Constants';
import AppointmentValidation from './AppointmentValidation';
const switfiTokenUrl = Constants.switfiTokenUrl;
const switfiAuthorization = Constants.switfiAuthorization;
const switfiContactsUrl = Constants.switfiContactsUrl;
const switfiAppointmentUrl = Constants.switfiAppointmentUrl;

class Appointment extends Component {
	constructor(props) {
		super(props);
		this.state = {
			daySelected: false,
			isAppointmentTaken: false,
			token: '',
			contactId: '',
			isContactPosted: false,
			contact: {
				gender: '',
				firstname: '',
				lastname: '',
				email: '',
				phoneNumber: '',
				origin: 'Site'
			},
			days: [],
			isHoursloaded: false,
			isDaysloading: false,
			isAppointmentSelected: false,
			appointments: [],
			selectedDay: '',
			selectedDayIndex: null,
			appointment: {
				datetime: '',
				espaceRdvSlotId: ''
			},
			// Date picker
			sliderGap: 0,
			visibleDaysNb: 5
		};
		this.onFormChange = this.onFormChange.bind(this);
		this.onAppointmentChange = this.onAppointmentChange.bind(this);
		this.onSubmitContact = this.onSubmitContact.bind(this);
		this.onSubmitAppointment = this.onSubmitAppointment.bind(this);
		this.onNextDay = this.onNextDay.bind(this);
		this.onPreviousDay = this.onPreviousDay.bind(this);
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}

	componentDidMount() {
		fetch(switfiTokenUrl, {
			method: 'POST',
			headers: {
				Authorization: switfiAuthorization,
				'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
			},
			body: this.xWwwFormUrlEncode({ grant_type: 'client_credentials' })
		})
			.then((response) => response.json())
			.then((data) => {
				this.setState({ token: data.access_token });
			});

		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	xWwwFormUrlEncode(obj) {
		return Object.keys(obj)
			.reduce((params, key) => {
				const values = !Array.isArray(obj[key]) ? [ obj[key] ] : obj[key];

				// We check if the array has more than one item to append [] to the key
				// in the url encoded form like this : key[]=value
				const hasMoreThanOneItem = values.length > 1;

				return params.concat(
					values.map(
						(value) =>
							`${encodeURIComponent(key)}${hasMoreThanOneItem ? '[]' : ''}=${encodeURIComponent(value)}`
					)
				);
			}, [])
			.join('&');
	}

	onFormChange(event) {
		const { name, value } = event.target;

		this.setState((prevState) => {
			let contact = Object.assign({}, prevState.contact);
			contact[name] = value;
			return { contact };
		});
	}

	onSubmitContact(event) {
		event.preventDefault();

		fetch(switfiContactsUrl, {
			method: 'POST',
			headers: {
				Authorization: 'Bearer ' + this.state.token,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(this.state.contact)
		})
			.then((response) => response.json())
			.then((data) => {
				this.setState({ contactId: data.id, isContactPosted: true });
				this.onFetchAppointmentDay();
			});
	}

	onFetchAppointmentDay() {
		const year = new Date().getFullYear();
		const month = new Date().getMonth() + 1;

		this.setState({ isDaysloading: true });

		fetch(switfiAppointmentUrl + this.state.contactId + '/espace_rdv/date_available/' + year + '-' + month, {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + this.state.token,
				'Content-Type': 'application/json'
			}
		})
			.then((response) => response.json())
			.then((data) =>
				this.setState({
					days: data,
					isDaysloading: false
				})
			);
	}

	onSelectDay(selectedDay, index) {
		this.setState({
			isHoursloaded: true,
			selectedDayIndex: index,
			selectedDay: selectedDay,
			isAppointmentSelected: false
		});

		fetch(switfiAppointmentUrl + this.state.contactId + '/espace_rdv/schedule_available/' + selectedDay, {
			method: 'GET',
			headers: {
				Authorization: 'Bearer ' + this.state.token,
				'Content-Type': 'application/json'
			}
		})
			.then((response) => response.json())
			.then((data) => {
				this.setState({
					appointments: data,
					isHoursloaded: false
				});
			});
	}

	onAppointmentChange(event) {
		const appointmentId = event.target.value;
		const appointments = this.state.appointments;
		const appointmentSelected = appointments.find((a) => a.id.toString() === appointmentId);
		const appointmentDate = this.state.selectedDay + ' ' + appointmentSelected.time + ':00';

		this.setState((prevState) => {
			let appointment = Object.assign({}, prevState.appointment);
			appointment.datetime = appointmentDate;
			appointment.espaceRdvSlotId = appointmentSelected.id;
			return {
				appointment,
				isAppointmentSelected: true
			};
		});
	}

	onSubmitAppointment(event) {
		event.preventDefault();

		fetch(switfiContactsUrl + '/' + this.state.contactId, {
			method: 'PUT',
			headers: {
				Authorization: 'Bearer ' + this.state.token,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ appointment: this.state.appointment })
		})
			.then((response) => response.json())
			.then((data) => {
				this.setState({ isAppointmentTaken: true });
				console.log('onSubmitAppointment =>', data);
			});
	}

	// DATE PICKER UI
	updateWindowDimensions() {
		if (window.innerWidth < 768) {
			this.setState({ visibleDaysNb: 3 });
		} else {
			this.setState({ visibleDaysNb: 5 });
		}
	}

	onNextDay() {
		if (this.state.sliderGap > -120 * this.state.days.length + 120 * this.state.visibleDaysNb) {
			this.setState((prevState) => {
				return { sliderGap: (prevState.sliderGap -= 120) };
			});
		}
	}

	onPreviousDay() {
		if (this.state.sliderGap < 0) {
			this.setState((prevState) => {
				return { sliderGap: (prevState.sliderGap += 120) };
			});
		}
	}

	render() {
		const Appointments = this.state.appointments.map((appointment, index) => {
			return (
				<div className="appointment__switch" key={index}>
					<input
						className="input form__input"
						type="radio"
						value={appointment.id}
						id={index}
						name="appointments"
						onChange={this.onAppointmentChange}
					/>
					<label className="appointment__switch__label" htmlFor={index}>
						{appointment.time}
					</label>
				</div>
			);
		});

		const Days = this.state.days.map((day, index) => {
			return (
				<div className="appointment__picker__day" key={index}>
					<div className="text-center" onClick={(e) => this.onSelectDay(day, index, e)}>
						<div className="appointment__picker__day__name">
							<Moment interval={0} format="dddd" date={day} />
						</div>
						<div
							className={`appointment__picker__day__month 
						${this.state.selectedDayIndex === index ? 'appointment__picker__day__month--selected' : ''}
						`}
						>
							<Moment interval={0} format="D MMMM" date={day} />
						</div>
					</div>
					{this.state.selectedDayIndex === index && this.state.isHoursloaded ? (
						<div className="lds-ellipsis purple m-auto">
							<div />
							<div />
							<div />
							<div />
						</div>
					) : null}
					{this.state.selectedDayIndex === index && !this.state.isHoursloaded ? (
						<div className="appointment__picker__hours">{Appointments}</div>
					) : null}
				</div>
			);
		});

		return !this.state.isAppointmentTaken ? (
			<div className="appointment">
				<div className="container">
					<h2 className="title appointment__title">
						Allez, on sort son agenda et fait avancer son projet d’acquisition !
					</h2>
					<p className="title appointment__hint">
						Notre équipe courtage est disponible pour vous, prenez un rendez-vous, on s’occupe du reste.
					</p>
				</div>
				<div className="loopBg" />
				<div className="container appointment__form__container">
					{/* FORM */}
					{!this.state.isContactPosted ? (
						<form className="appointment__form" onSubmit={this.onSubmitContact}>
							<div className="appointment__form__info">
								<label className="label mt-4">Civilité </label>
								<div className="appointment__switch f-50">
									<input
										className="input form__input "
										type="radio"
										value="Mme."
										id="female"
										name="gender"
										checked={this.state.contact.gender === 'Mme.'}
										onChange={this.onFormChange}
										required
									/>
									<label
										className="appointment__switch__label appointment__switch__label--switch"
										htmlFor="female"
									>
										Madame
									</label>
									<input
										className="input form__input"
										type="radio"
										value="M."
										id="male"
										name="gender"
										checked={this.state.contact.gender === 'M.'}
										onChange={this.onFormChange}
										required
									/>
									<label
										className="appointment__switch__label appointment__switch__label--switch"
										htmlFor="male"
									>
										Monsieur
									</label>
								</div>
								<div className="d-flex flex-wrap">
									<div className="f-50">
										<label className="label">Prénom</label>
										<input
											className="input form__input"
											type="text"
											name="firstname"
											placeholder="Chew"
											onChange={this.onFormChange}
											required
										/>
									</div>
									<div className="f-50">
										<label className="label">Nom</label>
										<input
											className="input form__input"
											type="text"
											placeholder="BACCA"
											name="lastname"
											onChange={this.onFormChange}
											required
										/>
									</div>
								</div>
								<div className="d-flex flex-wrap">
									<div className="f-50">
										<label className="label">Email</label>
										<input
											className="input form__input"
											type="email"
											placeholder="chew.bacca@wookie.com"
											name="email"
											onChange={this.onFormChange}
											required
										/>
									</div>
									<div className="f-50">
										<label className="label">Téléphone</label>
										<input
											className="input form__input form__input--phone"
											type="tel"
											pattern="[0-9]{10}"
											placeholder="0619101977"
											name="phoneNumber"
											onChange={this.onFormChange}
											required
										/>
									</div>
								</div>
							</div>
							<input className="btn btn__primary m-auto" type="submit" value="Choisir mon RDV" />
						</form>
					) : (
						<div className="appointment__date-picker">
							{/* Date picker */}
							<h2 className="title appointment__title appointment__title--sm">
								Choisissez la date et l’heure de votre rendez-vous téléphonique
							</h2>
							<p className="title appointment__hint appointment__hint--lg">
								durée du rendez-vous : 45 min
							</p>
							<form className="mb-5" onSubmit={this.onSubmitAppointment}>
								<div className="appointment__picker__container">
									<div className="appointment__picker">
										<button
											className="appointment__picker__btn"
											type="button"
											onClick={this.onPreviousDay}
										>
											<img src={arrow} alt="" />
										</button>
										<div className="appointment__picker__days">
											{!this.state.isDaysloading ? (
												<div
													className="appointment__picker__days__slider"
													style={{ transform: `translateX(${this.state.sliderGap}px)` }}
												>
													{Days}
												</div>
											) : null}
										</div>
										<button
											className="appointment__picker__btn"
											type="button"
											onClick={this.onNextDay}
										>
											<img src={arrow} alt="" />
										</button>
									</div>
									{this.state.isAppointmentSelected ? (
										<input
											className="btn btn__primary m-auto fadeIn04s"
											type="submit"
											value="Confirmer le rendez-vous"
										/>
									) : null}
								</div>
							</form>
						</div>
					)}
				</div>
			</div>
		) : (
			<AppointmentValidation dateTime={this.state.appointment.datetime} />
		);
	}
}

export default Appointment;
