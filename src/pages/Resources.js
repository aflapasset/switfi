import React, { Component } from 'react';
import Newsletter from '../components/Newsletter';
import PressSlider from '../components/PressSlider';

import downloadArrow from '../assets/icons/downloadArrow.svg';
import downloadBox from '../assets/icons/downloadBox.svg';
import laura from '../assets/imgs/press/laura.jpg';
import logos from '../resources/Logo Switfi.zip';
import photos from '../resources/Photos Switfi.zip';
import visuals from '../resources/Visuels Switfi.zip';
import fullKit from '../resources/Kit presse Switfi.zip';

class Press extends Component {
	render() {
		return (
			<div className="press">
				<div className="container">
					{/* RESSOURCES */}
					<div className="press__resources">
						<h2 className="title">Nos ressources à votre disposition</h2>
						<div className="row">
							<div className="col-12 col-lg-4">
								{/* LOGOS */}
								<a href={logos} className="block block__clickable press__resources__block logos">
									<div className="press__resources__gradient" />
									<div className="press__resources__footer">
										<div className="press__resources__icon">
											<img src={downloadArrow} alt="" className="press__resources__icon__arrow" />
											<img src={downloadBox} alt="" className="press__resources__icon__box" />
										</div>
										<div>
											<p className="press__resources__title">Télécharger le logo en HD</p>
											<p className="press__resources__text">Format PDF, 11Ko</p>
										</div>
									</div>
								</a>
							</div>

							{/* PHOTOS */}
							<div className="col-12 col-lg-4">
								<a href={photos} className="block block__clickable press__resources__block photos">
									<div className="press__resources__gradient" />
									<div className="press__resources__footer">
										<div className="press__resources__icon">
											<img src={downloadArrow} alt="" className="press__resources__icon__arrow" />
											<img src={downloadBox} alt="" className="press__resources__icon__box" />
										</div>
										<div>
											<p className="press__resources__title">Télécharger les photos en HD</p>
											<p className="press__resources__text">Format JPGEG, 27,8Mo</p>
										</div>
									</div>
								</a>
							</div>

							{/* VISUELS */}
							<div className="col-12 col-lg-4">
								<a href={visuals} className="block block__clickable press__resources__block visuals">
									<div className="press__resources__gradient" />
									<div className="press__resources__footer">
										<div className="press__resources__icon">
											<img src={downloadArrow} alt="" className="press__resources__icon__arrow" />
											<img src={downloadBox} alt="" className="press__resources__icon__box" />
										</div>
										<div>
											<p className="press__resources__title">Télécharger les visuels en HD</p>
											<p className="press__resources__text">Format PNG, 11,9Mo</p>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div className="d-flex justify-content-center mt-3 mt-lg-5">
							<a href={fullKit} className="btn btn__primary d-flex">
								Télécharger le kit complet (39,7Mo)
							</a>
						</div>
					</div>

					<Newsletter page="press" text="Swit’ News, restez connecté(e)" />

					{/* LAURA */}
					<div className="block block--contact">
						<img src={laura} alt="Laura BOUSQUET" className="block--contact__img d-none d-md-block" />
						<div className="block--contact__info">
							<h2 className="title mb-4 mb-md-0 w-100">
								Chers journalistes, vous avez des questions&nbsp;?
							</h2>
							<div className="d-flex w-100 align-items-center justify-content-center">
								<img src={laura} alt="Laura BOUSQUET" className="block--contact__img mr-3 d-md-none" />
								<div className="block--contact__text">
									<p className="block--contact__name">Laura BOUSQUET</p>
								</div>
							</div>
							<a
								href="mailto:marketing@switfi.fr?subject=Contacter%20le%20service%20presse"
								className="btn btn__secondary press__laura-btn"
							>
								Contacter le service presse
							</a>
						</div>
					</div>
				</div>
				<PressSlider title="Au cas où vous l’auriez manqué, on parle de nous !" />
			</div>
		);
	}
}

export default Press;
