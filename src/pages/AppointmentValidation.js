import React, { Component } from 'react';
import Moment from 'react-moment';
import 'moment/locale/fr';

import phone from '../assets/imgs/phone-validation.svg';

class AppointmentValidation extends Component {
	render() {
		return (
			<div className="appointmentValidation">
				<div className="loopBg" />
				<div className="container d-flex flex-column align-items-center text-center">
					<h2 className="title">
						Une bonne chose de faite, <br /> un email de confirmation vient de vous être envoyé !
					</h2>
					<div className="appointmentValidation__img">
						<img src={phone} alt="" />
					</div>
					<p className="appointmentValidation__text">
						Un conseiller Switfi vous appellera <br />
						<span className="mt-1">
							<b>
								<Moment interval={0} format="dddd DD MMMM" date={this.props.dateTime} />
								&nbsp;à&nbsp;
								<Moment interval={0} format="HH:mm" date={this.props.dateTime} />
							</b>
						</span>
						<br /> <br />
						Afin d’avoir une réponse précise, veuillez préparer les documents listés <br />
						dans l’email que nous venons de vous envoyer.
					</p>
				</div>
			</div>
		);
	}
}

export default AppointmentValidation;
