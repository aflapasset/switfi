import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import LogosSlider from '../components/LogosSlider';
import ImageHeader from '../components/ImageHeader';

import arrow from '../assets/icons/arrow.svg';
import fivemin from '../assets/icons/5min.svg';
import experts from '../assets/imgs/experts.png';
import expert from '../assets/imgs/expert.png';
import dragndrop from '../assets/icons/dragndrop.svg';

class Home extends Component {
	render() {
		return (
			<div className="home">
				<ImageHeader text="Estimer votre crédit immobilier n’a jamais été aussi simple" />

				<div className="container">
					{/* DESKTOP POINTS */}
					<section className="home__points d-none d-lg-flex justify-content-between">
						<Link to="/individual" className="block home__points__block">
							<img src={fivemin} alt="5 minutes" className="home__points__fivemin" />
							<div className="home__points__text">
								Une <b>réponse personnalisée</b> en 5 minutes et sans inscription
							</div>
							<div className="home__points__block__bar" />
						</Link>
						<Link to="/individual" className="block home__points__block">
							<img src={experts} alt="Experts" className="home__points__experts" />
							<div className="home__points__text">
								Nos <b>experts en financement</b> vous accompagnent jusqu’à la signature du prêt
							</div>
							<div className="home__points__block__bar" />
						</Link>
						<Link to="/individual" className="block home__points__block">
							<img src={dragndrop} alt="Drag and drop" className="home__points__dragndrop" />
							<div className="home__points__text">
								Un <b>espace sécurisé</b> pour obtenir votre prêt dans un temps record
							</div>
							<div className="home__points__block__bar" />
						</Link>
					</section>

					{/* MOBILE POINTS */}
					<section className="home__points home__points--mobile block d-flex flex-column d-lg-none">
						<Link to="/individual" className="home__points__line">
							<img src={fivemin} alt="Answer" className="home__points__line__img" />
							<p className="home__points__line__first-text">
								Une <b>réponse personnalisée</b> en 5 minutes et sans inscription
							</p>
						</Link>
						<Link to="/individual" className="home__points__line">
							<img src={expert} alt="Expert" className="home__points__line__img" />
							<p>
								Nos <b>experts en financement</b> vous accompagnent jusqu’à la signature du prêt
							</p>
						</Link>
						<Link to="/individual" className="home__points__line">
							<img src={dragndrop} alt="Drag and drop" className="home__points__line__img" />
							<p>
								Un <b>espace sécurisé</b> pour obtenir votre prêt dans un temps record
							</p>
						</Link>
					</section>

					<div className="d-flex justify-content-center">
						<Link to="/individual" className="link text-center">
							Découvrir le fonctionnement de Switfi en 4 étapes
							<img src={arrow} alt="arrow" className="link__arrow" />
						</Link>
					</div>

					<section className="home__users">
						<div className="row">
							<div className="col-12 col-lg-6">
								<Link to="/professional">
									<div className="block home__users__block" />
									<div className="block block__clickable home__users__text-block">
										<h3 className="home__users__title">Professionnels de l’immobilier</h3>
										Gagnez du temps avec les solutions Switfi
									</div>
								</Link>
							</div>
							<div className="col-12 col-lg-6">
								<Link to="/recruitment">
									<div className="block home__users__block home__users__block--whynotyou " />
									<div className="block block__clickable home__users__text-block">
										<h3 className="home__users__title">Pourquoi pas vous ?</h3>
										Rejoignez-nous, on recrute !
									</div>
								</Link>
							</div>
						</div>
					</section>
				</div>

				<section className="home__partners">
					<div className="home__partners__bg">
						<div className="home__partners__bg__confetti" />
					</div>
					<div className="home__partners__container">
						<h2 className="title mb-5">Nos partenaires pour vous aider à réaliser votre projet</h2>

						<LogosSlider logosNb={8} />
						{/* <div className="d-flex justify-content-center">
							<Link className="link text-center mt-4" to="/partner">
								Devenir partenaire de Switfi
								<img src={arrow} alt="arrow" className="link__arrow" />
							</Link>
						</div> */}
					</div>
				</section>

				<div className="container">
					<section className="home__last-cta">
						<p className="home__last-cta__text">
							Une réponse en 5 minutes et un conseiller qui m’accompagne
						</p>
						<Link to="/chatbot" className="btn btn__primary">
							Estimer mon crédit immobilier
						</Link>
					</section>
				</div>
			</div>
		);
	}
}

export default Home;
