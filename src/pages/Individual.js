import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ImageHeader from '../components/ImageHeader';
import LogosSlider from '../components/LogosSlider';
import OpinionsSlider from '../components/OpinionsSlider';
import PressSlider from '../components/PressSlider';
import ScrollAnimation from 'react-animate-on-scroll';
import Recommend from '../components/Recommend';
import Newsletter from '../components/Newsletter';

import Gauge from '../assets/icons/gauge.svg';
import Account from '../assets/icons/account.svg';
import Stopwatch from '../assets/icons/stopwatch.svg';
import Champagne from '../assets/icons/champagne.svg';
import File from '../assets/imgs/dragndrop/file.svg';

import Advisor1 from '../assets/imgs/advisors/1.png';
import Advisor2 from '../assets/imgs/advisors/2.png';
import Advisor3 from '../assets/imgs/advisors/3.png';
import Advisor4 from '../assets/imgs/advisors/4.png';
import Advisor5 from '../assets/imgs/advisors/5.png';
import Advisor6 from '../assets/imgs/advisors/6.png';

const opinionUrl = 'https://new.api.opinionsystem.fr/v2/company/rating?api_key=40a6a649cb9d8e29e2acecd79893baad';

class Individual extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rating: null,
			animateAdvisor: false
		};
	}

	componentDidMount() {
		// Get Opinion System API
		fetch(opinionUrl).then((response) => response.json()).then((data) =>
			this.setState({
				rating: data.company_rating[0].rating
			})
		);
	}

	render() {
		return (
			<div className="individual">
				<ImageHeader text="Trouvez votre crédit immobilier aux meilleures conditions en quelques clics" />
				<div className="container">
					<h2 className="title mt-5">Comment ça fonctionne ?</h2>

					{/* STEP 1 */}
					<ScrollAnimation
						animateIn="fadeIn"
						offset={100}
						animateOnce
						afterAnimatedIn={() => this.setState({ animateAdvisor: true })}
					>
						<div className="block block--step d-flex">
							<div className="block--step__step">
								<div className="block--step__step__number">1</div>
								<img src={Gauge} alt="Jauge" />
							</div>
							<div>
								<h3 className="block--step__title">J’estime mon crédit</h3>
								<p className="block--step__text">
									En ligne ou par téléphone, j’ai le résultat en quelques minutes. Un conseiller
									Switfi m’accompagne tout au long du projet
								</p>
								<div
									className={`individual__advisors d-flex ${this.state.animateAdvisor
										? 'individual__advisors--animate'
										: ''}
										`}
								>
									<div className="individual__advisor">
										<p className="individual__advisor__name">Léa</p>
										<img src={Advisor1} alt="Léa" className="individual__advisor__img" />
									</div>
									<div className="individual__advisor">
										<p className="individual__advisor__name">Jim</p>
										<img src={Advisor2} alt="Léa" className="individual__advisor__img" />
									</div>
									<div className="individual__advisor">
										<p className="individual__advisor__name">Sandrine</p>
										<img src={Advisor3} alt="Léa" className="individual__advisor__img" />
									</div>
									<div className="individual__advisor__group">
										<div className="individual__advisor">
											<p className="individual__advisor__name">...</p>
											<img src={Advisor4} alt="Léa" className="individual__advisor__img" />
										</div>
										<div>
											<img src={Advisor5} alt="Léa" className="individual__advisor__img" />
										</div>
										<div className="">
											<img src={Advisor6} alt="Léa" className="individual__advisor__img" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</ScrollAnimation>

					{/* STEP 2 */}
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block block--step block--step--step2 d-flex">
							<div className="block--step__step">
								<div className="block--step__step__number">2</div>
								<img src={Account} alt="Dossier" />
							</div>
							<div>
								<h3 className="block--step__title">Je complète mon dossier sur mon espace personnel</h3>
								<p className="block--step__text">
									Grâce à mon espace Switfi dédié, simple et intuitif, je transmets tous les documents
									nécessaires à mon financement en toute sécurité
								</p>
								<div className="individual__dragndrop-anim">
									<p className="individual__dragndrop-anim__text">Pièces à fournir</p>
									<div className="row">
										<div className="col-12 col-md-6 mb-3">
											<div className="individual__dragndrop-anim__input individual__dragndrop-anim__input--animated" />
										</div>
										<div className="col-12 col-md-6">
											<div className="individual__dragndrop-anim__input" />
										</div>
									</div>
									<img src={File} alt="Fichier" className="individual__dragndrop-anim__file" />
								</div>
							</div>
						</div>
					</ScrollAnimation>

					{/* STEP 3 */}
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block block--step d-flex flex-column overflow-hidden">
							<div className="d-flex">
								<div className="block--step__step">
									<div className="block--step__step__number">3</div>
									<img src={Stopwatch} alt="Dossier" />
								</div>
								<div>
									<h3 className="block--step__title">Je reçois des solutions de financement</h3>
									<p className="block--step__text">
										Nous vous présentons les propositions négociées pour vous avec nos partenaires
										bancaires. Choisissez celle qui vous conviendra le mieux.
									</p>
								</div>
							</div>
							<div className="individual__logos-slider">
								<p className="individual__logos-slider__text">
									Nous présentons votre dossier à nos partenaires bancaires.
								</p>
								<LogosSlider logosNb={5} />
							</div>
						</div>
					</ScrollAnimation>

					{/* STEP 4 */}
					<ScrollAnimation animateIn="fadeIn" offset={100} animateOnce>
						<div className="block block--step d-flex flex-column">
							<div className="d-flex">
								<div className="block--step__step">
									<div className="block--step__step__number">4</div>
									<img src={Champagne} alt="Champagne" />
								</div>
								<div>
									<h3 className="block--step__title">
										Je signe l’offre de prêt et ouvre le champagne
									</h3>
									<p className="block--step__text">
										Confirmez votre choix, votre conseiller Switfi s’occupe du reste. Ce qui vous
										laisse plus de temps pour choisir la future déco (et nous inviter à la
										crémaillère) !
									</p>
								</div>
							</div>
						</div>
						<div className="individual__letsgo d-flex flex-column align-items-center">
							<div className="home__partners__bg">
								<div className="home__partners__bg__confetti" />
							</div>
							<p className="individual__hint">Aller hop, c’est parti !</p>
							<Link to="/chatbot" className="btn btn__primary mt-4 mb-4">
								Estimer mon crédit immobilier
							</Link>
							<Link to="/appointment" className="btn btn__secondary">
								Prendre rdv avec un conseiller Switfi
							</Link>
						</div>
					</ScrollAnimation>

					<Recommend />
				</div>

				<OpinionsSlider />

				<PressSlider />
				<Newsletter
					page="individual"
					text="Suivez notre actualité et celle de l’immobilier avec notre newsletter"
				/>
			</div>
		);
	}
}

export default Individual;
