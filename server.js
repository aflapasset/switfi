const express = require('express');
const favicon = require('serve-favicon');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();
require('dotenv').config();
app.use(express.json());
app.use(favicon(path.join(__dirname, 'build', 'favicon.ico')));
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'build')));
app.get('/*', function(req, res) {
	res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

var nodemailer = require('nodemailer');

var transport = {
	host: 'smtp.gmail.com',
	port: '465',
	auth: {
		user: process.env.GMAIL_USER,
		pass: process.env.GMAIL_PASSWORD
	}
};

var transporter = nodemailer.createTransport(transport);

transporter.verify((error, success) => {
	if (error) {
		console.log(error);
	} else {
		console.log('Server is ready to take messages');
	}
});

app.post('/send', (req, res, next) => {
	var firstName = req.body.firstName;
	var lastName = req.body.lastName;
	var email = req.body.email;
	var phone = req.body.phone;
	var message = req.body.message;

	var content = `Prénom: ${firstName} \nNom: ${lastName} \nEmail: ${email}  \nTéléphone: ${phone}  \nMessage: ${message} `;

	var mail = {
		from: firstName,
		to: 'contact@switfi.fr', //Change to email address that you want to receive messages on
		subject: 'Switfi site contact',
		text: content
	};

	transporter.sendMail(mail, (err, data) => {
		res.json({
			err: err,
			data: data
		});
	});
});

app.listen(port);
